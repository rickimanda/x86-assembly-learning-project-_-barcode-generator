A project done in 2021 for the Computer Architecture (ECOAR) course. Task was to write a type 128 (character set A) barcode generator in x86 and C. Aim was to learn the basics of the x86 Assembly language and architecture, and learn to implement assembly into a C program. Individual project.

Included in the /description are the task requirements and 2 examples of generated barcodes.
