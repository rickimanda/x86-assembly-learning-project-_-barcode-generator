#ifndef BMP_H
#define BMP_H

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#pragma pack(push, 1)

typedef struct
{
	unsigned short bfType;	// 0x4D42
	unsigned long  bfSize;	// file size in bytes
	unsigned short bfReserved1;
	unsigned short bfReserved2;
	unsigned long  bfOffBits;	// offset of pixel data
	unsigned long  biSize;		// header size (bitmap info size)
	long  biWidth;			// image width
	long  biHeight;			// image height
	short biPlanes;			// bitmap planes (== 3)
	short biBitCount;		// bit count of a pixel (== 24)
	unsigned long  biCompression;	// should be 0 (no compression)
	unsigned long  biSizeImage;		// image size (not file size!)
	long biXPelsPerMeter;			// horizontal resolution
	long biYPelsPerMeter;			// vertical resolution
	unsigned long  biClrUsed;		// not imoprtant for RGB images
	unsigned long  biClrImportant;	// not imoprtant for RGB images
} RGBbmpHdr;

#pragma pack(pop)

typedef struct
{
	unsigned int width, height;
	unsigned int linebytes;
	unsigned char* pImg;
	RGBbmpHdr *pHeader;
} imgInfo;

imgInfo* allocImgInfo()
{
	imgInfo* retv = malloc(sizeof(imgInfo));
	if (retv != NULL)
	{
		retv->width = 0;
		retv->height = 0;
		retv->linebytes = 0;
		retv->pImg = NULL;
		retv->pHeader = NULL;
	}
	return retv;
}

void* freeImgInfo(imgInfo* toFree)
{
	if (toFree != NULL)
	{
		if (toFree->pImg != NULL)
			free(toFree->pImg);
		if (toFree->pHeader != NULL)
			free(toFree->pHeader);
		free(toFree);
	}
	return NULL;
}

void* freeResources(FILE* pFile, imgInfo* toFree)
{
	if (pFile != NULL)
		fclose(pFile);
	return freeImgInfo(toFree);
}

imgInfo* readBMP(const char* fname)
{
	imgInfo* pInfo = 0;
	FILE* fbmp = 0;

	if ((pInfo = allocImgInfo()) == NULL)
		return NULL;

	if ((fbmp = fopen(fname, "rb")) == NULL)
		return freeResources(fbmp, pInfo);  // cannot open file

	// read the header into a RGBbmpHdr called pHeader and point to it from pInfo -> pHeader
	if ((pInfo->pHeader = malloc(sizeof(RGBbmpHdr))) == NULL ||
		fread((void *)pInfo->pHeader, sizeof(RGBbmpHdr), 1, fbmp) != 1)
		return freeResources(fbmp, pInfo);

	// several checks - quite restrictive and only for RGB files
	if (pInfo->pHeader->bfType != 0x4D42 || pInfo->pHeader->biPlanes != 1 ||
		pInfo->pHeader->biBitCount != 24 || pInfo->pHeader->biCompression != 0)
		return (imgInfo*) freeResources(fbmp, pInfo);

	//read the rest of the image into pInfo -> pImg
	if ((pInfo->pImg = malloc(pInfo->pHeader->biSizeImage)) == NULL ||
		fread((void *)pInfo->pImg, 1, pInfo->pHeader->biSizeImage, fbmp) != pInfo->pHeader->biSizeImage)
		return (imgInfo*) freeResources(fbmp, pInfo);

	fclose(fbmp);
	pInfo->width = pInfo->pHeader->biWidth;
	pInfo->height = pInfo->pHeader->biHeight;
	pInfo->linebytes = pInfo->pHeader->biSizeImage / pInfo->pHeader->biHeight;

	printf("source bmp read\n");

	return pInfo;
}

int saveBMP(const imgInfo* pInfo, const char* fname)
{
	FILE * fbmp;
	if ((fbmp = fopen(fname, "wb")) == NULL)
		return -1;  // cannot open file for writing

	if (fwrite(pInfo->pHeader, sizeof(RGBbmpHdr), 1, fbmp) != 1  ||
		fwrite(pInfo->pImg, 1, pInfo->pHeader->biSizeImage, fbmp) != pInfo->pHeader->biSizeImage)
	{
		fclose(fbmp);  // cannot write header or image
		return -2;
	}

	fclose(fbmp);

	printf("output bmp saved\n");

	return 0;
}



#endif
