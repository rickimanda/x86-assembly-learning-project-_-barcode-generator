/*
author: Anna Krawczuk
date: 17.06.2021

Code for C file handling and memory allocation was based on teacher's x86 starter pack

*/

	/* basic structure of the program:
	
	call 	int encode128 (imgInfo* pImg, int bar_width, char *text)

	
	encode 128 {
		
		calls a c function int io_error_check (int bar_width, char* text)
			returns an int with an error id
			so that encode128 can return the same error id - as per requirements
			
		draw start pattern
		start checksum
			
		loop:
			
		calls a c function char* encode (char symbol)
			returns a pattern associated with the char
			
		calls asm function draw_pattern
		
		checksum
		
		exit loop when done
		
		end checksum calculations and draw checksum pattern
		draw stop pattern
	}	
	
	*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include "bmp.h"
#include "barcode.h"
#include "input.h"

// definitions of functions used in asm
extern void set_column(imgInfo* pImg, unsigned int col_idx, unsigned int color);
extern void draw_pattern(imgInfo* pImg, char* pattern, unsigned int width);
extern int encode128 (imgInfo* pImg, int bar_width, char *text);

char quiet_zone[] = "0000000000";
char start_pattern[12];
char stop_pattern[14];

void patterns_to_arrays()
{
	int i =0;
	
	char* temp = decimal_to_binary(start_decimal);
	while (*temp != '\0')
	{	start_pattern[i] = *temp;
		temp++;
		i++; }
	start_pattern[11] = '\0';
	
	i =0;
	temp = decimal_to_binary(stop_decimal);
	while (*temp != '\0')
	{	stop_pattern[i] = *temp;
		temp++;
		i++; }
	stop_pattern[13] = '\0';
}

	
int main(int argc, char *argv[])
{
	
	imgInfo* pInfo;
	patterns_to_arrays(); // because passing char* variables to asm doesn't work, but char[] does?????

	if (sizeof(RGBbmpHdr) != 54)
	{
		printf("Check compilation options so as RGBbmpHdr struct size is 54 bytes.\n");
		return 1;
	}
	if ((pInfo = readBMP("source.bmp")) == NULL)
	{
		printf("Error reading source file (probably).\n");
		return 2;
	}
	
	printf("Inputs: Text: %s, width: %d\n", INPUT_TEXT, INPUT_WIDTH);
	
	// error handling
	
	if ( 	strlen(INPUT_TEXT) < 1 ||
			strlen(INPUT_TEXT) >50 ||
			INPUT_WIDTH <1 ||
			INPUT_WIDTH >8 )
			{
				printf("Input variables not correct\n");
				freeResources(NULL, pInfo);
				return 3;
			}
			
	else if ( (strlen(INPUT_TEXT)*11+55)*INPUT_WIDTH > 600 )
			{
				printf("Too wide to fit this text\n");
				freeResources(NULL, pInfo);
				return 4;
			}
	else {		
	
	int encode_result = encode128 (pInfo, INPUT_WIDTH, INPUT_TEXT);
	
	
	saveBMP(pInfo, "output.bmp");
	freeResources(NULL, pInfo);
	
	return 0; }
	
	return 0;
}

