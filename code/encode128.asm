;	typedef struct
;	{
;		unsigned int width, height;
;		unsigned int linebytes;
;		unsigned char* pImg;
;		RGBbmpHdr *pHeader;
;	} imgInfo;

; imgInfo structure layout
img_width				EQU 0
img_height			EQU 4
img_linebytes		EQU 8
img_pImg				EQU	12
img_RGBbmpHdr	EQU 16 ; not really used

extern start_pattern
extern stop_pattern
extern quiet_zone
extern encode
extern symbol_value
extern checksum_encode

section .data

x	DW 0					; current x coordinate

section	.text
global  	set_column
global 	draw_pattern
global	encode128

;==============================================================
; ENCODE128
; arguments: 4 pointer to img info ; 4 width ; 4 pointer to text to be encoded
; returns: 0 if executed correctly ; positive int when error

encode128:
	
	push 	ebp
	mov		ebp, esp

	; arguments on stack are at:
	; ebp+8 -> pointer to imgInfo struct , ebp+12-> width , ebp+16 -> pointer to text to be encoded
	
	; first thing - call the c function that checks inputs
	
	; first, draw the quiet zone and the start pattern
	; I don't care about the registers yet so no pushing
	
	mov		eax, [ebp+12]
	push	eax						; width
	mov		ecx, quiet_zone			; pattern string
	push	ecx
	mov		ebx, [ebp+8]		; pointer to imgInfo struct
	push	ebx
	call		draw_pattern
	add		esp, 12
	
	mov		eax, [ebp+12]
	push	eax						; width
	mov		ecx, start_pattern			; pattern string
	push	ecx
	mov		ebx, [ebp+8]		; pointer to imgInfo struct
	push	ebx
	call		draw_pattern
	add		esp, 12
	
	; registers used in this function:
	; esi -> pointer to beggining of string ; bl -> first char in the string
	; ecx for checksum ; edx for position counter
	mov		esi, [ebp +16]
	mov		ebx, dword 0
	mov		bl, [esi]
	
	; init checksum
	mov 	ecx, dword 104		; start pattern B value
	mov		edx, dword 1

	; begin loop
	iter_patterns:
	
	; go to end_patterns when string ends - bl has value 0
	cmp		bl, byte 0
    je 		end_patterns
	
	; save registers
	push	esi
	
	push	ecx
	push	edx
	push	ebx
					
	; call encode (char)				
	push	ebx				; current char to be encoded
	call		encode
	add		esp, 4
	
	; call draw_pattern
	mov		ecx, [ebp+12]			; width
	push	ecx						
	push	eax						; pattern string - output of encode which was saved to eax
	mov		ebx, [ebp+8]			; pointer to imgInfo struct
	push	ebx
	call		draw_pattern
	add		esp, 12
	
	; restore registers
	
	pop		ebx
	; get current char and call symbol_value - for checksum
	push	ebx				; current char 
	call		symbol_value
	add		esp, 4
	push	ebx
	
	pop		ebx
	pop 		edx
	pop		ecx
	pop		esi
	
	; calculations for checksum
	
	imul		eax, edx		; multiply value (eax) by position (edx) and store in eax
	add		ecx, eax 		; add the val*pos (eax) to checksum (ecx)
	add		edx, dword 1	; add to position counter
	
	; iterate
	add		esi, dword 1 				; iterate pointer
	mov		ebx, dword 0
	mov		bl, [esi]				; set edi to next char in the string
	jmp		iter_patterns


	end_patterns:
	
	; checksum calculations
	; div - eax divident low half ; edx =0 high half ; divisor any register
	; div ebx divides eax by ebx
	; returns: edx remainder ; eax quotient
	
	mov		edi, 103
	mov 	eax, ecx
	xor		edx, edx
	div 		edi
	
	; call c function that returns checksum pattern
	push	edx
	call		checksum_encode
	add		esp, 4
	
	; call draw_pattern
	mov		ecx, [ebp+12]			; width
	push	ecx						
	push	eax						; pattern string - output of encode which was saved to eax
	mov		ebx, [ebp+8]			; pointer to imgInfo struct
	push	ebx
	call		draw_pattern
	add		esp, 12
	
	; draw stop pattern and quiet zone
	
	mov		eax, [ebp+12]
	push	eax						; width
	mov		ecx, stop_pattern			; pattern string
	push	ecx
	mov		ebx, [ebp+8]		; pointer to imgInfo struct
	push	ebx
	call		draw_pattern
	add		esp, 12
	
	mov		eax, [ebp+12]
	push	eax						; width
	mov		ecx, quiet_zone			; pattern string
	push	ecx
	mov		ebx, [ebp+8]		; pointer to imgInfo struct
	push	ebx
	call		draw_pattern
	add		esp, 12


	pop 		ebp
	ret


;==============================================================
; DRAW_PATTERN
; arguments: 4 pointer to img info ; 4 pointer to pattern string ; 4 width
; returns: nothing

draw_pattern:
	
	push 	ebp
	mov		ebp, esp

	; arguments on stack are at:
	; ebp+8 -> pointer to imgInfo struct , ebp+12-> pointer to pattern string , ebp+16 -> width
	
	; esi -> pointer to beggining of string ; bl -> first char in the string  ; ax -> current x coord ; ecx ->counter for width loop
	mov		esi, [ebp +12]
	mov		bl, [esi]
	mov		ax, [x]
	
	next_char:
	
	; go to end_pattern when string ends - bl has value 0
	cmp		bl, byte 0
    je 		end_pattern
	
	; starting point for the width loop
	mov		ecx, dword 0
	
	width_loop:
	cmp 	ecx, [ebp+16]
	je			width_loop_end
	
	; go to draw_white when current char = '0'
	cmp		bl, 0x30
	je 		draw_white
	
	; this happens when there is a 1 in the string
	
	; first, I want eax, ebx and esi to not change value 
	; since I use them in this function, so I put them on stack
	push	esi
	push	eax
	push	ebx
	push	ecx
	
	; call set_column
	; arguments first to last: 4 pointer to img info ; 4 column id ; 4 color
	; eax and ebx are used just for the call
	push	0x00000000		; black
	mov		eax, [x]
	push	eax						; current colunm / x coordinate
	mov		ebx, [ebp+8]
	push	ebx						; pointer to imgInfo struct, from the function arguments
	call		set_column
	add		esp, 12
	
	; pop eax, esi and edi so they have the same value as before the call
	pop		ecx
	pop		ebx
	pop		eax
	pop		esi
	
	jmp		next_column
	
	; this happens when there is a 0 in the string
	draw_white:
	
	push	esi
	push	eax
	push	ebx
	push	ecx
	
	push	0x00FFFFFFF		; white
	mov		eax, [x]
	push	eax						
	mov		ebx, [ebp+8]
	push	ebx
	call		set_column
	add		esp, 12
	
	pop		ecx
	pop		ebx
	pop		eax
	pop		esi
	
	next_column:
	add		ax, 1				
	mov		[x], ax		; increase x coordinate
	
	; width loop iteration
	add		ecx, dword 1
	jmp 		width_loop
	
	width_loop_end:
	
	add		esi, dword 1 				; iterate pointer
	mov		bl, [esi]				; set edi to next char in the string
	
	jmp 		next_char
	
	end_pattern:


	pop 		ebp
	ret
	

;==============================================================
; SET_COLUMN
; arguments: 4 pointer to img info ; 4 column id ; 4 color
; returns: 1 if no error

set_column:
	push 	ebp
	mov		ebp, esp

	
	mov		eax, [ebp + 8]	; eax <- address of imgInfo struct

	; computing address of pixel (0, col_idx)
	mov 	edi, [eax + img_pImg]
	mov 	ecx, [ebp + 12]	; ecx <- col_idx
	lea 		ecx, [2 * ecx + ecx]	; ecx *= 3 really :)
	add 		edi, ecx
	
	mov 	ebx, [ebp + 16] ; ebx <- color
	mov 	ecx, [eax + img_height]
	
set_pixel_in_column:
	; essentially set pixel 
	mov 	[edi], bl
	ror 		ebx, 8
	mov 	[edi + 1], bl
	ror 		ebx, 8
	mov 	[edi + 2], bl
	
	; color register is now rotated 16 bits - restoring
	ror 		ebx, 16
	
	; move destination addres to the next row
	add 		edi, [eax + img_linebytes]
	
	loop set_pixel_in_column
	

	pop	ebp
	ret
